<?php

/**
 * @file
 * uw_ct_project.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_project_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-management_project-settings:admin/config/system/uw_ct_project_settings.
  $menu_links['menu-site-management_project-settings:admin/config/system/uw_ct_project_settings'] = array(
    'menu_name' => 'menu-site-management',
    'link_path' => 'admin/config/system/uw_ct_project_settings',
    'router_path' => 'admin/config/system/uw_ct_project_settings',
    'link_title' => 'Project settings',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-management_project-settings:admin/config/system/uw_ct_project_settings',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment.
  $menu_links['menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/strategic_alignment',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Strategic alignment',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_strategic-alignment:admin/structure/taxonomy/strategic_alignment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Project settings');
  t('Strategic alignment');

  return $menu_links;
}
