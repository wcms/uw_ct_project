/**
 * @file
 */

(function ($) {
  $(function () {
    // Grouping field Nr.1 in view only provides H3, we have to change this H3 to H2.
    $('.view-id-uw_project_by_topic_page .view-content').find('h3').not('.project-title').replaceWith(function () {return '<h2>' + $(this).html() + '</h2>';
    });
    $('.view-id-uw_project_by_audience_page .view-content').find('h3').not('.project-title').replaceWith(function () {return '<h2>' + $(this).html() + '</h2>';
    });
  });
}(jQuery));
