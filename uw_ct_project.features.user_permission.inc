<?php

/**
 * @file
 * uw_ct_project.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_project_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer project configuration'.
  $permissions['administer project configuration'] = array(
    'name' => 'administer project configuration',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_ct_project',
  );

  // Exported permission: 'create uw_project content'.
  $permissions['create uw_project content'] = array(
    'name' => 'create uw_project content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_project content'.
  $permissions['delete any uw_project content'] = array(
    'name' => 'delete any uw_project content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_project content'.
  $permissions['delete own uw_project content'] = array(
    'name' => 'delete own uw_project content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_project content'.
  $permissions['edit any uw_project content'] = array(
    'name' => 'edit any uw_project content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_project content'.
  $permissions['edit own uw_project content'] = array(
    'name' => 'edit own uw_project content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_project revision log entry'.
  $permissions['enter uw_project revision log entry'] = array(
    'name' => 'enter uw_project revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_project authored by option'.
  $permissions['override uw_project authored by option'] = array(
    'name' => 'override uw_project authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_project authored on option'.
  $permissions['override uw_project authored on option'] = array(
    'name' => 'override uw_project authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_project promote to front page option'.
  $permissions['override uw_project promote to front page option'] = array(
    'name' => 'override uw_project promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_project published option'.
  $permissions['override uw_project published option'] = array(
    'name' => 'override uw_project published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_project revision option'.
  $permissions['override uw_project revision option'] = array(
    'name' => 'override uw_project revision option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_project sticky option'.
  $permissions['override uw_project sticky option'] = array(
    'name' => 'override uw_project sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_project content'.
  $permissions['search uw_project content'] = array(
    'name' => 'search uw_project content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'search_config',
  );

  // Exported permission: 'show format selection for field_collection_item'.
  $permissions['show format selection for field_collection_item'] = array(
    'name' => 'show format selection for field_collection_item',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'better_formats',
  );

  return $permissions;
}
