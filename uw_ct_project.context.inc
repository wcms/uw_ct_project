<?php

/**
 * @file
 * uw_ct_project.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_project_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'uw_project';
  $context->description = 'Displays search box, projects by status, topic,  audience and strategic alignment';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'projects' => 'projects',
        'projects/*' => 'projects/*',
        'all_current_projects' => 'all_current_projects',
        'all_other_initiatives' => 'all_other_initiatives',
        'all_proposed_projects' => 'all_proposed_projects',
        'all_completed_projects' => 'all_completed_projects',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'uw_ct_project-project_by_status' => array(
          'module' => 'uw_ct_project',
          'delta' => 'project_by_status',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'uw_ct_project-project_by_topic' => array(
          'module' => 'uw_ct_project',
          'delta' => 'project_by_topic',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
        'uw_ct_project-project_by_audience' => array(
          'module' => 'uw_ct_project',
          'delta' => 'project_by_audience',
          'region' => 'sidebar_second',
          'weight' => '-7',
        ),
        'views-00bd0b45f85b11b0c8fbea4c2b51b8e9' => array(
          'module' => 'views',
          'delta' => '00bd0b45f85b11b0c8fbea4c2b51b8e9',
          'region' => 'sidebar_second',
          'weight' => '-6',
        ),
        'uw_ct_project-project_search' => array(
          'module' => 'uw_ct_project',
          'delta' => 'project_search',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays search box, projects by status, topic,  audience and strategic alignment');
  $export['uw_project'] = $context;

  return $export;
}
